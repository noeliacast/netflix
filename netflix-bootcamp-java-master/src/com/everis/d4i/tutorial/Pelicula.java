package com.everis.d4i.tutorial;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Pelicula extends Contenido
{
	
	private String _nombreDirector;
	private int _anoPublicacion;
	
	Calendar cal = Calendar.getInstance();
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	public Pelicula() 
	{
		// TODO Auto-generated constructor stub
	}
	
	public Pelicula (String nombre, Date fechaReproduccion, Double valoracion, int idContenido, String nombreDirector, int anoPublicacion)
	{
		super(nombre, fechaReproduccion, valoracion, idContenido);
		_nombreDirector = nombreDirector;
		_anoPublicacion = anoPublicacion;
	}

	public String get_nombreDirector() 
	{
		return _nombreDirector;
	}

	public void set_nombreDirector(String _nombreDirector) 
	{
		this._nombreDirector = _nombreDirector;
	}

	public int get_aņoPublicacion() 
	{
		return _anoPublicacion;
	}

	public void set_aņoPublicacion(int anoPublicacion) 
	{
		this._anoPublicacion = anoPublicacion;
	}
	
	
	
	@Override
	public String toString() 
	{
		return "Pelicula: " + "\n ID Contenido: " + get_idContenido() + "\n Nombre = " + get_nombre() + "\n Director: = " + _nombreDirector + "\n Valoracion: " + get_valoracion() 
				  + "\n Fecha reproduccion = " + dateFormat.format(get_fechaReproduccion())  + "\n Aņo publicacion: " + _anoPublicacion + "\n";
	}

}
