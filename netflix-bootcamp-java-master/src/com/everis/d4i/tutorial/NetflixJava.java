package com.everis.d4i.tutorial;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class NetflixJava {
	public static Date date = new Date();
	public static Calendar cal = Calendar.getInstance();
	public static ArrayList<Object> contenido = new ArrayList<Object>();

	public static void main(String[] args) {
		ArrayList<Pelicula> pelis = new ArrayList<Pelicula>();
		HashMap<Integer, Double> hash = new HashMap<>();

		Pelicula p1 = new Pelicula("The clockwork orange", cal.getTime(), null, 1, "Cubrick", 1971);
		Documental d1 = new Documental("the Blue Planet", date, null, 2, "Pallares", 2001);
		Pelicula p3 = new Pelicula("The Notebook", date, null, 4, "Cassavetes", 2004);
		Pelicula p4 = new Pelicula("The Shining", cal.getTime(), 10.0, 4, "Stephen King", 1980);
		Pelicula p5 = new Pelicula("Pet Sematary", cal.getTime(), 8.0, 5, "Stephen King", 1989);
		Serie s2 = new Serie("Stranger Things", date, null, hash, 3, "Algo", 5, 2010);

		anadirContenido(hash, pelis, p1, d1, s2);
		modificarAtributo(hash, s2, p1);
		borrarYValorar(p3);
		anadirFecha(p4, p5);
		filtrar(pelis);
	}

	public static void anadirContenido(HashMap<Integer, Double> hash, ArrayList<Pelicula> pelis, Pelicula p1,
			Documental d1, Serie s2) {
		System.out.println(
				"--Recientemente he visto la peli The clockwork orange de Cubrick, el documental the Blue Planet y Stranger Things. No las he valorado por el momento. (incializaci�n)--");

		for (int i = 1; i < s2.get_numTemporadas(); i++) {
			hash.put(i, null);
		}

		contenido.add(p1);
		contenido.add(d1);
		contenido.add(s2);

		for (int i = 0; i < contenido.size(); i++) {
			System.out.println(contenido.get(i).toString());
		}
	}

	private static void modificarAtributo(HashMap<Integer, Double> hash, Serie s2, Pelicula p1) {
		System.out.println("\n --La serie me gusta pero la temporada 2 no:--");
		hash.put(2, 1.0);

		s2.set_valTemporada(hash);
		s2.set_valoracion(6.0);
		System.out.println(s2);

		System.out.println("\n --La naranja mecanica es de Kubrick. Lo corrijo:--");
		p1.set_nombreDirector("Kubrick");
		System.out.println(p1);
	}

	private static void borrarYValorar(Pelicula p3)

	{
		System.out.println("\n --Mi hermana me roba la cuenta de Netflix y mira the Notebook (Y lo registra)--");
		System.out.println(p3);

		System.out.println(
				"\n --Revisando el registro, me doy cuenta de ello y lo borro inmediatamente. Doy valoraciones a las pel�culas que no tienen--");
		System.out.println("Pelicula borrada: \n" + p3 + "\n");
		contenido.remove(p3);

		System.out.println("\n Nuevas valoraciones:");

		for (Object object : contenido) {
			if (object instanceof Pelicula) {
				if (((Pelicula) object).get_valoracion() == null) {
					double val = Math.round(Math.random() * 100) / 10.0;
					((Pelicula) object).set_valoracion(val);
					System.out.println(object.toString());
				}
			}
		}
	}

	private static void anadirFecha(Pelicula p4, Pelicula p5) {
		System.out.println(
				"\n --Ayer hice marat�n de Stephen King y vi the Shining y Pet Sematary. Ambas me gustaron mucho, aunque la primera m�s que la segunda.--");
		cal.add(Calendar.DATE, -1);

		contenido.add(p4);
		contenido.add(p5);

		for (int i = 0; i < contenido.size(); i++) {
			System.out.println(contenido.get(i).toString());
		}
	}

	private static void filtrar(ArrayList<Pelicula> pelis) {
		System.out.println("\n --Filtro las peliculas por valoraci�n: solo busco las que me han gustado m�s.--");

		for (Object object : contenido) {
			if (object instanceof Pelicula && ((Pelicula) object).get_valoracion() > 7.0) {
				pelis.add((Pelicula) object);
			}
		}

		/*
		 * List<Pelicula> listaContenido = pelis;
		 * 
		 * Collections.sort(listaContenido, new Comparator<Pelicula>() {
		 * 
		 * @Override public int compare(Pelicula p1, Pelicula p2) { return
		 * p2.get_valoracion().compareTo(p1.get_valoracion()); } });
		 * 
		 * System.out.println(listaContenido);
		 */

		List<Pelicula> listaPelis = contenido.stream().filter(x -> x instanceof Pelicula).map(x -> (Pelicula) x)
				.filter(x -> x.get_valoracion() > 7.0).sorted(Comparator.comparingDouble(Pelicula::get_valoracion))
				.collect(Collectors.toList());
		/* .forEach(System.out::println); */

		System.out.println(listaPelis);

	}
}