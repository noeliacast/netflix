package com.everis.d4i.tutorial;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Contenido {
	
	private String _nombre;
	private Date _fechaReproduccion;
	private Double _valoracion;
	private int _idContenido;

	public Contenido() {
		// TODO Auto-generated constructor stub
	}
	
	public Contenido (String nombre, Date fechaReproduccion, Double valoracion, int idContenido) 
	{
		_nombre = nombre;
		_fechaReproduccion = fechaReproduccion;
		_valoracion = valoracion;
		_idContenido = idContenido;
	}

	public String get_nombre() {
		return _nombre;
	}

	public void set_nombre(String _nombre) {
		this._nombre = _nombre;
	}

	public Date get_fechaReproduccion() {
		return _fechaReproduccion;
	}

	public void set_fechaReproduccion(Date _fechaReproduccion) {
		this._fechaReproduccion = _fechaReproduccion;
	}

	public Double get_valoracion() {
		return _valoracion;
	}

	public void set_valoracion(Double _valoracion) {
		this._valoracion = _valoracion;
	}

	public int get_idContenido() {
		return _idContenido;
	}

	public void set_idContenido(int _idContenido) {
		this._idContenido = _idContenido;
	}
	
}
