package com.everis.d4i.tutorial;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Serie extends Contenido {

	private String _nombreEstudio;
	private int _numTemporadas;
	private int _anoTemporada;
	private Map<Integer, Double> _valTemporada;
	Calendar cal = Calendar.getInstance();
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//    LocalDate ld = LocalDate.now()

	public Serie() {
		// TODO Auto-generated constructor stub
	}

	public Serie(String nombre, Date fechaReproduccion, Double valoracionGlobal, HashMap<Integer, Double> valTemporada,
			int idContenido, String nombreEstudio, int numTemporadas, int anoTemporada) {
		super(nombre, fechaReproduccion, valoracionGlobal, idContenido);
		_valTemporada = new HashMap<>(valTemporada);
		_nombreEstudio = nombreEstudio;
		_numTemporadas = numTemporadas;
		_anoTemporada = anoTemporada;
	}

	public String get_nombreEstudio() {
		return _nombreEstudio;
	}

	public void set_nombreEstudio(String _nombreEstudio) {
		this._nombreEstudio = _nombreEstudio;
	}

	public int get_numTemporadas() {
		return _numTemporadas;
	}

	public void set_numTemporadas(int _numTemporadas) {
		this._numTemporadas = _numTemporadas;
	}

	public int get_a�oTemporada() {
		return _anoTemporada;
	}

	public void set_a�oTemporada(int anoTemporada) {
		this._anoTemporada = anoTemporada;
	}

	public Map<Integer, Double> get_valTemporada() {
		return _valTemporada;
	}

	public void set_valTemporada(HashMap<Integer, Double> _valTemporada) {
		this._valTemporada = _valTemporada;
	}

	@Override
	public String toString() {
		return "Serie: \n ID Contenido: " + get_idContenido() + "\n Nombre: " + get_nombre() + "\n Estudio: "
				+ _nombreEstudio + "\n Temporadas: " + _numTemporadas + "\n Valoracion temporada: " + _valTemporada
				+ "\n Valoracion global: " + get_valoracion() + "\n Fecha reproduccion: "
				+ dateFormat.format(get_fechaReproduccion()) + "\n A�o de primera temporada: " + _anoTemporada + "\n";
	}

}
